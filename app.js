var express = require('express');
var http = require('http');
var path = require("path");
var app = express();
var server = app.listen(8000);
var io = require('socket.io').listen(server);
var Sonos = require('sonos').Sonos;

app.set('view engine', 'ejs');

var sonos = new Sonos('192.168.2.14', 1400);
var data;
var prevData = {
    track: '',
    volume: ''
};


app.get('/', function (req, res) {
    res.render(path.join(__dirname + '/views/index.ejs'), {data: data});
});

console.log("Running at Port 8000");

function updateQueue() {
    sonos.getQueue(function (err, queue) {
        io.emit('queue', {data: queue});
    });
}

sonos.deviceDescription(function (err, info) {
    io.emit('sonosinfo', {data: info.roomName});
});

io.on('connection', function (socket) {

    io.emit('volume', {data: prevData.volume});


    socket.on('back', function () {
        sonos.previous(function (err, something) {
            console.log(err, something);
        });
    });
    socket.on('pause', function () {
        sonos.pause(function (err, something) {
            console.log(err, something);
        });
    });
    socket.on('play', function () {
        sonos.play(function (err, something) {
            console.log(err, something);
        });
    });
    socket.on('forward', function () {
        sonos.next(function (err, something) {
            console.log(err, something);
        });
    });
    socket.on('volumeChange', function (volume) {
        sonos.setVolume(volume, function (err, something) {
            console.log(err, something);
        });
    });
});

setInterval(function () {
    sonos.currentTrack(function (err, track) {
        data = track;
        if (prevData.track !== track.title) {
            prevData.track = track.title;
            console.log('[NEW TRACK]', track.title);
            updateQueue();
            io.emit('currenttrack', {data: track});
        }
    });

    sonos.getVolume(function (err, vol) {
        if (prevData.volume !== vol) {
            prevData.volume = vol;
            console.log(vol);
            io.emit('volume', {data: vol});
        }
    });
}, 1500);